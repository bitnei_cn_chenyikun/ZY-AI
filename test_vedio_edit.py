import numpy as np
from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip, ImageClip

# 加载绿幕视频
video = VideoFileClip('I:\PycharmProjects\\AI-BOTS\\video\\数字人_女2号_多动作_tts_Easy-Wav2Lip.mp4')

# 加载背景图片（确保图片尺寸与视频一致或者调整大小）
background_image = ImageClip('D:\Downloads\\3DGaussianSplatting.jpg', duration=video.duration)
background_image = background_image.resize(video.size)

# 设置文字
#txt_clip = TextClip("test title", fontsize=70, color='white')
#txt_clip = txt_clip.set_pos(('center', 'top')).set_duration(video.duration)

# 创建一个函数，用于删除绿色背景
def remove_green_screen(image):
    # 对图像数据进行复制以避免尝试修改只读的数组。
    image_copy = np.copy(image)
    green_threshold = 100
    image_copy[(image[:,:,1] > green_threshold) & (image[:,:,:].sum(axis=2) < 600)] = 0
    return image_copy

# 应用函数删除绿幕背景，并将每一帧的图像转换为有透明度的图像
video = video.fl_image(remove_green_screen)

# 合成视频
comp_clip = CompositeVideoClip([background_image, video])

# 输出最终视频
comp_clip.write_videofile('I:\PycharmProjects\\AI-BOTS\\video\\数字人_女2号_多动作_tts_Easy-Wav2Lip_2.mp4', codec='libx264', audio_codec='aac')