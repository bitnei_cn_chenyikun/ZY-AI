import gradio as gr
import threading
import time


def process_video(video_path):
    # 模拟视频处理时间
    time.sleep(5)
    # 假设处理后的视频路径
    processed_video_path = "/path/to/processed/video.mp4"
    return processed_video_path


def on_button_click(video_path):
    # 创建一个线程来处理视频
    thread = threading.Thread(target=lambda: process_video(video_path))
    thread.start()

    # 线程启动后，设置一个回调函数，用于在线程执行完毕后更新界面
    def update_interface():
        processed_path = thread.join()  # 确保线程执行完毕
        gr.update("video_player", value=processed_path)  # 更新视频播放器的路径

    gr.update("status", value="Processing...")  # 更新状态信息
    gr.register_update("video_player", update_interface)  # 注册更新回调


# 创建 Gradio 界面
iface = gr.Interface(fn=lambda x: x,
                     inputs=gr.inputs.Video(label="Upload Video"),
                     outputs=gr.outputs.Video(label="Processed Video", elem_id="video_player"),
                     live=True)

# 添加一个按钮
iface.register_button("Process Video", on_button_click)

# 启动应用
iface.launch()