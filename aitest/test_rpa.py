import gradio as gr


# 按钮1的处理函数
def on_button1_click(data):
    # 按钮1的逻辑处理
    result = f"按钮1已点击，数据为： {data}"
    # 手动触发按钮2的点击事件
    button2.click()
    return result


# 按钮2的处理函数
def on_button2_click(data):
    # 按钮2的逻辑处理
    return f"按钮2已点击，数据为： {data}"


with gr.Blocks() as demo:
    # 创建输入组件
    input_text = gr.Textbox(label="输入数据")
    # 创建按钮
    button1 = gr.Button("按钮1")
    button2 = gr.Button("按钮2")

    # 绑定按钮1的点击事件
    button1.click(fn=on_button1_click, inputs=input_text, outputs="text")


    # 绑定按钮2的点击事件，注意这里我们不能直接调用button2.click，
    # 因为button2.click()需要一个gr.Interface的上下文来注册点击事件。
    # 我们通过定义一个函数并用button2.click来绑定这个函数。
    def trigger_button2(data):
        return on_button2_click(data)


    button2.click(fn=trigger_button2, inputs=input_text, outputs="text")

    # 显示结果的Label
    output = gr.Label()

# 启动应用
demo.launch()