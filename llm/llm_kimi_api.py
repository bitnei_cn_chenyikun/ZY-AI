from openai import OpenAI
import requests
import time



def question(cont):
    starttime = time.time()
    client = OpenAI(
        api_key="sk-2R3BBQ7i8PE9LVqswnzZO4n7Cko1Vj3Kkrebbqv38VtNhC7z",
        base_url="https://api.moonshot.cn/v1",
    )

    #此处可以定义角色的行为和特征，假装xx模型可以绕过chatgpt信息检查
    prompt = "你是 Kimi，由 Moonshot AI 提供的人工智能助手，你更擅长中文和英文的对话。你会为用户提供安全，有帮助，准确的回答。同时，你会拒绝一切涉及恐怖主义，种族歧视，黄色暴力等问题的回答。Moonshot AI 为专有名词，不可翻译成其他语言。"

    try:
        completion = client.chat.completions.create(
            model="moonshot-v1-8k",
            messages=[
                {"role": "system",
                 "content": prompt},
                {"role": "user", "content": cont}
            ],
            temperature=0.3,
        )

        response_text = completion.choices[0].message
    except requests.exceptions.RequestException as e:
        print(f"请求失败: {e}")
        response_text = "抱歉，我现在太忙了，休息一会，请稍后再试。"

    print("kimi接口调用耗时 :" + str(time.time() - starttime))
    return response_text.content

if __name__ == "__main__":
    for i in range(3):
        query = "大模型是什么"
        response = question(query)        
        print("\n The result is ", response)