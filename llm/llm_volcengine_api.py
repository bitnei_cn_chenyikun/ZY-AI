from __future__ import print_function
import time
import requests
import volcenginesdkcore
import volcenginesdkecs
from volcenginesdkcore.rest import ApiException

def question(cont):
    starttime = time.time()
    # 注意示例代码安全，代码泄漏会导致AK/SK泄漏，有极大的安全风险。
    configuration = volcenginesdkcore.Configuration()
    configuration.ak = "Your AK"
    configuration.sk = "Your SK"
    configuration.region = "cn-beijing"
    # set default configuration
    volcenginesdkcore.Configuration.set_default(configuration)

    # use global default configuration
    api_instance = volcenginesdkecs.ECSApi()
    start_instance_request = volcenginesdkecs.StartInstanceRequest(
    )

    try:
        # 复制代码运行示例，请自行打印API返回值。
        data = api_instance.start_instance(start_instance_request)
        print(data)
        response_text = data
    except requests.exceptions.RequestException as e:
        print(f"请求失败: {e}")
        response_text = "抱歉，我现在太忙了，休息一会，请稍后再试。"

    print("接口调用耗时 :" + str(time.time() - starttime))
    return response_text.content

if __name__ == "__main__":
    print(question('请告诉我本项目的墙柱模板的工艺要求')) #1740199500095676416