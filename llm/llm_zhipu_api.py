from zhipuai import ZhipuAI
import requests
import time

def question(cont):
    starttime = time.time()

    try:
        client = ZhipuAI(api_key="4eaadc433c2bc20ab3423740431bca6f.4KVFYhKIiSCogVU7")  # 请填写您自己的APIKey
        response = client.chat.completions.create(
            model="glm-3-turbo",  # 填写需要调用的模型名称 glm-4、glm-3-turbo
            messages=[
                {"role": "user",  "content": cont},
            ],
            #tools=[
            #    {
            #        "type": "retrieval",
            #        "retrieval": {
            #            "knowledge_id": "1740199500095676416",
            #            "prompt_template": "从文档\n\"\"\"\n{{knowledge}}\n\"\"\"\n中找问题\n\"\"\"\n{{question}}\n\"\"\"\n的答案，找到答案就仅使用文档语句回答问题，找不到答案就用自身知识回答并且告诉用户该信息不是来自文档。\n不要复述问题，直接开始回答。"
            #        }
            #    }
            #],
            stream=False,
        )
        response_text = response.choices[0].message
    except requests.exceptions.RequestException as e:
        print(f"请求失败: {e}")
        response_text = "抱歉，我现在太忙了，休息一会，请稍后再试。"

    print("接口调用耗时 :" + str(time.time() - starttime))
    return response_text.content

if __name__ == "__main__":
    print(question('请告诉我本项目的墙柱模板的工艺要求')) #1740199500095676416