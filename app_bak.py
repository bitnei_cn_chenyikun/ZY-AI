import concurrent
import configparser
import json
import math
import random
from string import Template
import os
import subprocess
import time
import speech_recognition as sr
import librosa
import argparse
import globals
import gradio as gr
from langchain_core.tools import Tool

import qa_service
from ui import flask_server
from asr_vost import audio_input, audio_input_whisper
from core import content_db
from llm import llm_rwkv_api, llm_zhipu_api, llm_kimi_api, llm_gpt_api
from run import run_easy_wab2lip, SUPPORTED_VEDIO
from systools import wsa_server, util
from tts.TTS import EdgeTTS
from tts.asr_whisper import asr_whisper
from tts.ttsx import ttsx_say
from pathlib import Path
from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip, ImageClip


def btn_llm_input_click(llm_module, msg):
    return globals.get_instance().llm_talk(llm_module, msg)

def send_human(text):

    audio_file, sub_file = globals.get_instance().tts.predict(text, "zh-CN-YunjianNeural", 0, 100, 0, "Video\\tts.wav", "Video\\tts.vtt")

    file_url =  util.current_working_directory + "\\video\\tts.wav"
    file_text = text
    audio_length = ""
    say_type = "interact"

    content = {'Topic': 'Unreal',
               'Data': {'Key': 'audio', 'Value': file_url, 'Text': file_text, 'Time': audio_length, 'Type': say_type}}
    wsa_server.get_instance().add_cmd(content)
    print("发送msg完成")


def btn_generateAudio_click(text, voice, rate, volume, pitch):
    audio_file, sub_file = globals.get_instance().tts.predict(text, voice, rate, volume, pitch, "Video\\tts.wav", "Video\\tts.vtt")
    print(text, audio_file, sub_file)
    send_human(text)
    return audio_file

def btn_generateVedio_click(vedio_name, vedio_quality, selected_output_height, selected_wav2lip_version, txt_u, txt_d, txt_l, txt_r, txt_size, txt_feathering):
    save_config = configparser.ConfigParser()
    save_config.read('config.ini')
    save_config['OPTIONS']['quality'] = vedio_quality
    save_config['OPTIONS']['output_height'] = selected_output_height
    save_config['OPTIONS']['wav2lip_version'] = selected_wav2lip_version
    save_config['PADDING']['U'] = str(txt_u)
    save_config['PADDING']['D'] = str(txt_d)
    save_config['PADDING']['L'] = str(txt_l)
    save_config['PADDING']['R'] = str(txt_r)
    save_config['MASK']['size'] = str(txt_size)
    save_config['MASK']['feathering'] = str(txt_feathering)
    save_config.write(open("config.ini", "w"))

    output_video = run_easy_wab2lip(vedio_name, vedio_quality)
    print("数字人生成成功,{}".format(output_video))
    return output_video

def btn_audio_input_click(llm_model_key, chat_history, chk_chat_tts, chatbot_web, video_play):
    message = audio_input()
    return respond(llm_model_key, message, chat_history, chk_chat_tts, chatbot_web, video_play)

def respond(llm_model_key, message, chat_history, chk_chat_tts, chatbot_web, video_play):
    bot_message, action_json = btn_llm_input_click(llm_model_key, message)

    if action_json:
        if action_json['type'] == 'url':
            #如果action类型是url则读取对应的data到浏览器中打开
            url = action_json['data']
            chatbot_web = "<iframe src='" + url + "' width='100%' height='400px'></iframe>"
        if action_json['type'] == 'sql':
            #如果action类型是sql则按照要求打开数据源并读取对应数据然后返回结果
            url = action_json['data']
        if action_json['type'] == 'global':
            #如果action类型是global则是调用公共变量的模板，采用templete方式读取  "My name is $name, and I am $age years old."
            template = Template(action_json['data'])
            bot_message = template.substitute(**globals.get_instance().system_data)
        if action_json['type'] == 'post':
            #如果action类型是post则按照posthead和postdata要求进行请求指定的posturl,返回的结果进行读取和解析，并且使用语言模板进行结果合并并返回用户
            url = action_json['data']
        if action_json['type'] == 'agent':
            #如果action类型是agent则按照tool agent的方式调用并返回结果
            agent_name = action_json['agent_name']
            agent_para = action_json['para']
            agent = globals.get_instance().get_agent_by_name(agent_name)
            if agent != None:
                agent_result = agent.run(agent_para)
                bot_message = '[Agent-' + agent_name +']：'+ agent_result

    chat_history.append((message, bot_message))
    message = '';

    future2 = globals.get_instance().executor.submit(send_human, bot_message)

    if chk_chat_tts:
        future = globals.get_instance().executor.submit(ttsx_say, bot_message)



    return message, bot_message, chat_history, chatbot_web, video_play


# 替换为你想要显示的文件夹路径
video_folder_path = 'D:\Downloads'
thumbnails_folder_path = 'D:\Downloads'

# 确保缩略图文件夹存在
os.makedirs(thumbnails_folder_path, exist_ok=True)

def extract_thumbnail(video_file):
    video_path = os.path.join(video_folder_path, video_file)
    thumbnail_path = os.path.join(thumbnails_folder_path, os.path.splitext(video_file)[0] + ".jpg")

    # 如果缩略图不存在，则使用ffmpeg从视频中抽取
    if not os.path.isfile(thumbnail_path):
        cmd = [
            "ffmpeg", "-i", video_path,
            "-ss", "00:00:01.000",  # 从第1秒开始
            "-vframes", "1",
            thumbnail_path
        ]
        subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    return thumbnail_path

def get_video_files():
    video_files = [
        file for file in os.listdir(video_folder_path)
        if file.lower().endswith(('.mp4', '.mkv', '.avi'))  # 支持的视频格式
    ]
    return video_files


def btn_begin_vedio2word_click(select_radio_file):
    return asr_whisper("medium", video_folder_path+'\\'+select_radio_file)

def btn_SaveProps_click(txt_ProjectValue, txt_StructuralConstructionProgress, txt_MechanicalProgress, txt_InteriorFinishingProgress):
    globals.system_data['ProjectValue'] = txt_ProjectValue
    globals.system_data['StructuralConstructionProgress'] = txt_StructuralConstructionProgress
    globals.system_data['MechanicalProgress'] = txt_MechanicalProgress
    globals.system_data['InteriorFinishingProgress'] = txt_InteriorFinishingProgress


def main(config):
    #读取配置
    video_file = config['OPTIONS']['video_file']
    vocal_file = config['OPTIONS']['vocal_file']
    quality = config['OPTIONS']['quality']
    output_height = config['OPTIONS']['output_height']
    wav2lip_version = config['OPTIONS']['wav2lip_version']
    use_previous_tracking_data = config['OPTIONS']['use_previous_tracking_data']
    nosmooth = config.getboolean('OPTIONS', 'nosmooth')
    U = config.getint('PADDING', 'U')
    D = config.getint('PADDING', 'D')
    L = config.getint('PADDING', 'L')
    R = config.getint('PADDING', 'R')
    size = config.getfloat('MASK', 'size')
    feathering = config.getint('MASK', 'feathering')
    mouth_tracking = config.getboolean('MASK', 'mouth_tracking')
    debug_mask = config.getboolean('MASK', 'debug_mask')
    batch_process = config.getboolean('OTHER', 'batch_process')

    contentdb = content_db.new_instance()
    contentdb.init_db()

    # 数字人互动服务----------------------------------begin
    ws_server = wsa_server.new_instance(port=10002)
    #ws_server.start_server()
    print("初始化ws_server完成")

    web_ws_server = wsa_server.new_web_instance(port=10003)
    #web_ws_server.start_server()
    print("初始化web_ws_server完成")
    # 数字人互动服务----------------------------------end

    # Agent服务----------------------------------begin
    #flask_server.start()
    # Agent服务----------------------------------end

    #绘制gradio
    with gr.Blocks(title=globals.get_instance().title, theme=gr.themes.Soft()) as demo:
        gr.Markdown("<center><H1>AI人工智能平台</H1></center>")
        with gr.Tabs():
            with gr.TabItem("BIM+GPT"):
                with gr.Row():
                    gr.Markdown("## 大模型对话（含企业私有知识库）")
                with gr.Row():
                    with gr.Accordion("参数配置",
                                      open=False,
                                      visible=True) as parameter_LLM:
                        chk_chat_tts = gr.Checkbox(label='是否语音播报', value=True)
                        llm_model_key = gr.Dropdown(globals.get_instance().llm_modules_cmb, label="选择大模型", value='llm_rwkv_api')
                with gr.Row():
                    with gr.Column(min_width=700):
                        chatbot = gr.Chatbot(label="大模型对话", height=500)  # 对话框
                    with gr.Column(min_width=300):
                        video_play = gr.Video(label="数字人视频", height=500)
                with gr.Row():
                    msg = gr.Textbox(label="输入")  # 输入文本框
                with gr.Row():
                    btn_audio_input = gr.Button("语音输入")  # 输入文本框
                with gr.Row():
                    clear = gr.ClearButton([msg, chatbot])  # 清除按钮
                    # 绑定输入框内的回车键的响应函数
                with gr.Row():
                    chatbot_web = gr.HTML(
                        "<iframe src='http://www.qmodel.cn/viewer/index.html?model=%E7%8F%A0%E6%B5%B7%E5%B8%82%E6%96%97%E9%97%A8%E5%8C%BA%E4%BF%9D%E9%9A%9C%E6%80%A7%E4%BD%8F%E6%88%BF%E4%BA%8C%E6%9C%9F%E5%B7%A5%E7%A8%8B_5%E6%A0%8B%E7%BB%93%E6%9E%84.rvt' width='100%' height='400px'></iframe>")
                with gr.Row():
                    txt_llm_output = gr.Text(label="LLM大模型回答", visible=False)
                    msg.submit(respond, [llm_model_key, msg, chatbot, chk_chat_tts, chatbot_web], [msg, txt_llm_output, chatbot, chatbot_web])
                    btn_audio_input.click(btn_audio_input_click, inputs=[llm_model_key, chatbot, chk_chat_tts, chatbot_web],
                                          outputs=[msg, txt_llm_output, chatbot, chatbot_web])
                with gr.Row():
                    with gr.Column():
                        gr.Markdown("## 音频生成")
                        voice = gr.Dropdown(globals.get_instance().tts.SUPPORTED_VOICE, label="Voice to be used", value='zh-CN-YunjianNeural')
                        with gr.Accordion("参数配置",
                                          open=False,
                                          visible=True) as parameter_article:
                            rate = gr.Slider(minimum=-100,
                                             maximum=100,
                                             value=0,
                                             step=1.0,
                                             label='Rate')
                            volume = gr.Slider(minimum=0,
                                               maximum=100,
                                               value=100,
                                               step=1,
                                               label='Volume')
                            pitch = gr.Slider(minimum=-100,
                                              maximum=100,
                                              value=0,
                                              step=1,
                                              label='Pitch')
                        audio = gr.Audio(label="Audio file")
                        btn_generate = gr.Button("生成音频", variant="primary")
                        btn_generate.click(btn_generateAudio_click,
                                           inputs=[txt_llm_output, voice, rate, volume, pitch],
                                           outputs=[audio],
                                           )
                    with gr.Column():
                        gr.Markdown("## 数字人互动")
                        txt_vedio_name = gr.Text(label="真人视频样本", value=video_file)
                        with gr.Accordion("参数配置",
                                          open=False,
                                          visible=True) as parameter_video:
                            selected_vedio_quality = gr.Radio(['Fast', 'Improved', 'Enhanced'], label="视频质量", value=quality)
                            selected_output_height = gr.Radio(['full resolution', 'half resolution'],
                                                           label='分辨率选项', value=output_height)  # output_height
                            selected_wav2lip_version = gr.Radio(['Wav2Lip', 'Wav2Lip_GAN'],
                                                                label='Wav2Lip版本类型', value=wav2lip_version)  # wav2lip_version

                            txt_u = gr.Slider(label="嘴部mask上边缘", minimum=-10, maximum=10, step=1, value=U)
                            txt_d = gr.Slider(label="嘴部mask下边缘", minimum=-10, maximum=10, step=1, value=D)
                            txt_l = gr.Slider(label="嘴部mask左边缘", minimum=-10, maximum=10, step=1, value=L)
                            txt_r = gr.Slider(label="嘴部mask右边缘", minimum=-10, maximum=10, step=1, value=R)
                            txt_size = gr.Slider(label="mask尺寸", minimum=0, maximum=5, step=0.5, value=size)
                            txt_feathering = gr.Slider(label="mask羽化", minimum=0, maximum=5, step=1, value=feathering)

                        btn_generate_video = gr.Button("生成数字人视频", variant="primary")
                        btn_generate_video.click(btn_generateVedio_click,
                                                 inputs=[txt_vedio_name, selected_vedio_quality, selected_output_height,
                                                         selected_wav2lip_version, txt_u, txt_d, txt_l, txt_r,
                                                         txt_size, txt_feathering],
                                                 outputs=[video_play]
                                                 )
            with gr.TabItem("项目参数设置"):
                with gr.Row():
                    txt_ProjectValue = gr.Textbox(label='项目产值(万)', value=globals.get_instance().system_data['ProjectValue'])
                with gr.Row():
                    gr.Markdown("## 进度1")
                    txt_P1Name = gr.Textbox(label='进度名称')
                    txt_StructuralConstructionProgress1 = gr.Textbox(label='土建结构进度(%)')
                    txt_MechanicalProgress1 = gr.Textbox(label='机电安装进度(%)')
                    txt_InteriorFinishingProgress1 = gr.Textbox(label='装饰装修进度(%)')
                with gr.Row():
                    gr.Markdown("## 进度2")
                    txt_P2Name = gr.Textbox(label='进度名称')
                    txt_StructuralConstructionProgress2 = gr.Textbox(label='土建结构进度(%)')
                    txt_MechanicalProgress2 = gr.Textbox(label='机电安装进度(%)')
                    txt_InteriorFinishingProgress2 = gr.Textbox(label='装饰装修进度(%)')
                with gr.Row():
                    gr.Markdown("## 进度3")
                    txt_P3Name = gr.Textbox(label='进度名称')
                    txt_StructuralConstructionProgress3 = gr.Textbox(label='土建结构进度(%)')
                    txt_MechanicalProgress3 = gr.Textbox(label='机电安装进度(%)')
                    txt_InteriorFinishingProgress3 = gr.Textbox(label='装饰装修进度(%)')
                with gr.Row():
                    gr.Markdown("## 进度4")
                    txt_P4Name = gr.Textbox(label='进度名称')
                    txt_StructuralConstructionProgress4 = gr.Textbox(label='土建结构进度(%)')
                    txt_MechanicalProgress4 = gr.Textbox(label='机电安装进度(%)')
                    txt_InteriorFinishingProgress4 = gr.Textbox(label='装饰装修进度(%)')
                with gr.Row():
                    gr.Markdown("## 进度5")
                    txt_P5Name = gr.Textbox(label='进度名称')
                    txt_StructuralConstructionProgress5 = gr.Textbox(label='土建结构进度(%)')
                    txt_MechanicalProgress5 = gr.Textbox(label='机电安装进度(%)')
                    txt_InteriorFinishingProgress5 = gr.Textbox(label='装饰装修进度(%)')
                with gr.Row():
                    gr.Markdown("## 进度6")
                    txt_P6Name = gr.Textbox(label='进度名称')
                    txt_StructuralConstructionProgress6 = gr.Textbox(label='土建结构进度(%)')
                    txt_MechanicalProgress6 = gr.Textbox(label='机电安装进度(%)')
                    txt_InteriorFinishingProgress6 = gr.Textbox(label='装饰装修进度(%)')
                with gr.Row():
                    btn_SaveProps = gr.Button('保存参数')
                    btn_SaveProps.click(fn=btn_SaveProps_click
                                        , inputs=[txt_ProjectValue
                                            , txt_StructuralConstructionProgress1
                                            , txt_MechanicalProgress1
                                            , txt_InteriorFinishingProgress1]
                                        )

            with gr.TabItem("文案"):
                with gr.Row():
                    gr.Markdown("## 文案提取")
                with gr.Row():
                    with gr.Column():
                        select_radio_file = gr.Radio(label="选择本地视频源", choices=get_video_files())
                    with gr.Column(scale=2):
                        image_display = gr.Image(label='视频预览图', interactive=False, height=400)
                    with gr.Column(scale=1):
                        txt_vedio_url = gr.Text(label='输入抖音、快手文案地址')

                # 定义当Radio选项改变时的动作
                def update_image(selected_file):
                    return extract_thumbnail(selected_file)

                # 绑定Radio组件的选择改变事件到update_image函数
                select_radio_file.change(fn=update_image, inputs=select_radio_file, outputs=image_display)

                with gr.Row():
                    btn_begin_vedio2word = gr.Button('开始文案提取')
                with gr.Row():
                    txt_vedio2word_result = gr.Text(label='提取文案结果')
                with gr.Row():
                    btn_begin_rewirte = gr.Button('文案改写（原创）')
                with gr.Row():
                    txt_rewirte_result = gr.Text(label='提取文案结果')

                btn_begin_vedio2word.click(fn=btn_begin_vedio2word_click, inputs=[select_radio_file],outputs=[txt_vedio2word_result])
    return demo

if __name__ == "__main__":
    globals.new_instance()
    parser = argparse.ArgumentParser()
    parser.add_argument("--server_port", type=int, default=7860)
    opt = parser.parse_args()

    config = configparser.ConfigParser()
    config.read('config.ini')


    demo = main(config)
    demo.queue()
    demo.launch(server_name="0.0.0.0", server_port=8090)