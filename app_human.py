import gradio as gr


def process_input(user_input):
    # 这里可以添加处理逻辑
    return "您输入的是: " + user_input


# 创建自定义布局
with gr.Blocks() as demo:
    with gr.Row():
        # 使用gr.Video组件加载视频
        video = gr.Video("video/cyk.mp4", elem_id="video-background", autoplay=True)
        # 添加输入组件
        input_box = gr.Textbox()

    with gr.Row(elem_id="overlay"):
        with gr.Column(variant="center", elem_id="overlay-content"):
            # 添加显示组件
            text_display = gr.Textbox(placeholder="文本将在这里显示", value="这里是文字", elem_id="text-display")
            image_display = gr.Image(elem_id="image-display")

# 添加自定义CSS
demo.css = """
#video-background {
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: fixed;
    top: 1000;
    left: 0;
    z-index: -1;
}
#overlay {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background: rgba(0, 0, 0, 0.5);
    color: white;
    padding: 20px;
    border-radius: 10px;
    z-index: 10;
}
#overlay-content {
    text-align: center;
}
video::-webkit-media-controls {
    display:none !important;
}
"""

demo.queue()
demo.launch(server_name="127.0.0.1", server_port=8091)