import json
import speech_recognition as sr
import tempfile
import wave

from tts.asr_whisper import asr_whisper


def audio_input_whisper():
    # 创建语音识别器实例
    recognizer = sr.Recognizer()

    with sr.Microphone() as source:
        print("开始录音...")
        audio = recognizer.listen(source)
        print('录音结束，识别中...')
    try:
        # 将AudioData转换为wav格式的音频数据
        audio_wav_data = audio.get_wav_data()
        temp_file = save_buffer_to_file(audio_wav_data)
        text = asr_whisper("medium", temp_file)
        print(text)
    except sr.UnknownValueError:
        text = "无法识别音频"
    except sr.RequestError:
        text = "无法获取结果"

    return text

def save_buffer_to_file(buffer):
        temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".wav", dir="cache_data")
        wf = wave.open(temp_file.name, 'wb')
        wf.setparams((1, 2, 16000, 0, 'NONE', 'not compressed'))
        wf.writeframes(buffer)
        wf.close()
        return temp_file.name

def audio_input():
    # 创建语音识别器实例
    recognizer = sr.Recognizer()

    with sr.Microphone() as source:
        print("开始录音...")
        audio = recognizer.listen(source)
        print('录音结束，识别中...')
    try:
        print('加载模型，语音推理识别中...')
        text = recognizer.recognize_vosk(audio, language='zh-cn') #vosk下载的语音模型解压后须改文件夹名为“model”
        data = json.loads(text)
        text = data["text"].replace(" ", "")
        print('推理完成...')
        print(text)
    except sr.UnknownValueError:
        text = "无法识别音频"
    except sr.RequestError:
        text = "无法获取结果"

    return text

if __name__ == "__main__":
    #audio_input_whisper()
    audio_input()