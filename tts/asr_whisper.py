# pip install -U openai-whisper
import math
import os
import time
import whisper
import arrow

# 定义模型、音频地址、录音开始时间
# model_name: base medium large large-v2
def asr_whisper_exec(model_name,file_path,start_time,initial_prompt):
    tm = time.time()
    # 替换文件的扩展名为.txt
    text_file_path = os.path.splitext(file_path)[0] + '.txt'
    if os.path.isfile(text_file_path):
        print('该视频已经提取过，直接返回已有提取记录')
        with open(text_file_path, 'r') as file:
            return file.read()

    model = whisper.load_model(model_name)
    result = model.transcribe(file_path,initial_prompt=initial_prompt, language='Chinese')
    for segment in result["segments"]:
        now = arrow.get(start_time)
        start = now.shift(seconds=segment["start"]).format("HH:mm:ss")
        end = now.shift(seconds=segment["end"]).format("HH:mm:ss")
        print("【"+start+"->" +end+"】："+segment["text"])
    txt_result = result['text']
    # 写入提供的内容到文本文件
    with open(text_file_path, 'w', encoding='utf-8') as file:
        file.write(txt_result)
    print('处理完成. 耗时: {} ms'.format(math.floor((time.time() - tm) * 1000)))
    return txt_result

def asr_whisper(model_name, file_path):
    return asr_whisper_exec(model_name, file_path, "2024-01-01 00:00:00", "以下是普通话的句子")

if __name__ == '__main__':
    print(asr_whisper("medium","J:\\PycharmProjects\\AI-BOTS\\video\\tts.wav"))

