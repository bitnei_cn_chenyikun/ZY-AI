from win32com.client import Dispatch

def tts_spai_say(msg):
    speaker = Dispatch("SAPI.SpVoice")
    speaker.Speak(msg)
    del speaker

if __name__ == "__main__":
    msg = "Python由荷兰数学和计算机科学研究学会的吉多·范罗苏姆于1990年代初设计，作为一门叫做ABC语言的替代品。"
    tts_spai_say(msg)