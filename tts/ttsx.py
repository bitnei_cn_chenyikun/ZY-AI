import pyttsx3 as pyttsx

from vosk import Model, KaldiRecognizer
import os
import wave
import json  # 用于将识别结果字符串转换为Python字典

def ttsx_say(msg, video_play):

    if video_play=="" or video_play==None:
        engine = pyttsx.init()
        engine.setProperty('rate', 180)  # 设置语音播报速度，一般115比较合适。
        engine.setProperty('volume', 1.0)  # 设置音量，level  between 0 and 1，默认是1。

        # getting details of current voice
        voices = engine.getProperty('voices')
        # changing index, changes voices. o for male。中度的女音，播报中、英文。
        print(voices)
        engine.setProperty('voice', voices[0].id)
        # changing index, changes voices. 1 for female。高亮的女音，只播报英文，中文未播报。
        # engine.setProperty('voice', voices[1].id)

        engine.say(msg)

        # Saving Voice to a file，On linux make sure that 'espeak' and 'ffmpeg' are installed
        # engine.save_to_file('测试结束', 'test.mp3')

        engine.runAndWait()




if __name__ == "__main__":
    ttsx_say('测试一下')