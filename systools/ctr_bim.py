import json
import urllib.parse


def generate_url(params):
    base_url = "http://127.0.0.1:8080//routeParams"
    url = "{}?{}".format(base_url, urllib.parse.urlencode(params))
    return url

if __name__ == '__main__':
    params = {
        "modelName": "167594419801656",
        "nodes": "[{title: 'F2'}]"
    }
    url = generate_url(params)
    print(url)