import os

from moviepy.video.VideoClip import ImageClip
from moviepy.video.compositing.CompositeVideoClip import CompositeVideoClip
from moviepy.video.io.VideoFileClip import VideoFileClip

'''
# 指定目标文件夹路径，确保目标文件夹存在
    current_directory = os.getcwd()  # 获取当前工作目录
    target_directory = os.path.join(current_directory, 'video')

    # 确保目标文件夹存在
    os.makedirs(target_directory, exist_ok=True)

    # 指定目标文件路径
    target_path = os.path.join(target_directory, 'cyk_app.mp4')

    # 指定修改背景的目标文件路径
    bg_target_path = os.path.join(target_directory, 'bg_cyk_app.mp4')

    # 指定修改背景图片路径
    bgpic_target_path = os.path.join(target_directory, 'bg.jpg')

    # 执行拷贝操作
    try:
        shutil.copy(video, target_path)
        print(f"文件已成功拷贝到 {target_path}")

        output_bg_video_path = composite_background_with_moviepy(bgpic_target_path, target_path, bg_target_path)
        print(f"插入背景图片的视频成功拷贝到 {output_bg_video_path}")

    except Exception as e:
        print(f"拷贝文件时发生错误: {e}")

    return target_path
'''
#把视频加上指定的背景图，覆盖全视频
def composite_background_with_moviepy(background_path, video_path, output_video_path):
    # 加载视频
    video_clip = VideoFileClip(video_path)

    # 加载背景图片，并设置持续时间与视频相同
    background_clip = ImageClip(background_path).set_duration(video_clip.duration)

    # 将背景图片放置在视频的每一帧上
    # set_position() 方法可以设置图片的位置，例如 'center' 表示居中
    final_clip = CompositeVideoClip([video_clip, background_clip.set_position(("center", "center"))],
                                    size=video_clip.size)

    # 写入输出视频
    final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac', fps=video_clip.fps)

    # 释放资源
    final_clip.close()
    video_clip.close()
    background_clip.close()

    return output_video_path


'''
# 使用示例
video_path = 'path_to_your_video.mp4'
# 图片路径、插入时间、持续时间和位置的列表
image_paths_with_times_and_positions = [
    ('path_to_image1.jpg', 10, 5, ('center', 'center')),  # 在第10秒处插入，持续5秒，居中
    ('path_to_image2.png', 20, 5, ('center', 'center')),  # 在第20秒处插入，持续5秒，居中
    # 添加更多图片和时间...
]
output_video_path = 'path_to_your_output_video.mp4'

# 调用函数
insert_multiple_images(video_path, images_with_times, output_video_path)
print(f"多个图片已插入到视频，并保存到: {output_video_path}")
'''
#将多个图片加入到多个视频中
def insert_multiple_images(video_path, image_paths_with_times_and_positions, output_video_path):
    # 加载视频
    video = VideoFileClip(video_path)

    # 视频的总帧数
    total_duration = video.duration

    # 创建一个空的剪辑列表用于存储最终视频剪辑
    clips = []

    # 添加原始视频剪辑
    clips.append(video.subclip(0, total_duration))

    # 对于每张图片，创建一个剪辑并添加到列表
    for image_path, insertion_time, duration, position in image_paths_with_times_and_positions:
        # 创建图片剪辑
        image_clip = ImageClip(image_path).set_duration(duration)

        # 淡入时长，这里设置为1秒
        fade_duration = 1

        # 计算图片剪辑的结束时间，并创建一个剪辑，包含视频的一部分和图片
        end_time = insertion_time + duration
        video_part = video.subclip(max(0, insertion_time - fade_duration), end_time)

        # 将图片放在视频上方
        composite = CompositeVideoClip([video_part, image_clip.set_position(position)], size=video.size)

        # 添加淡入效果
        clips.append(composite)

    # 将所有剪辑合并为最终视频
    final_clip = CompositeVideoClip(clips, size=video.size, method="compose")

    # 写入输出视频
    final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac', fps=video.fps)

    # 释放资源
    video.close()
    final_clip.close()



'''
# 使用示例
video_path = 'path_to_your_video.mp4'
image_path = 'path_to_your_image.jpg'
insertion_time = 10  # 视频的第10秒处插入图片
position = ('center', 'center')  # 图片位置设置为视频中心
duration = 5  # 图片持续显示5秒
output_video_path = 'path_to_your_output_video.mp4'

# 调用函数
insert_image_at_time(video_path, image_path, insertion_time, position, duration, output_video_path)
print(f"图片已插入到视频，并保存到: {output_video_path}")
'''
#在视频指定的位置加上图，并写入视频
def insert_image_at_time(video_path, image_path, insertion_time, position, duration, output_video_path):
    # 加载视频
    with VideoFileClip(video_path) as video_clip:
        # 加载图片并设置持续时间
        image_clip = ImageClip(image_path)
        image_clip = image_clip.set_duration(duration)

        # 设置图片在视频中的位置
        image_clip = image_clip.set_position(position)

        # 创建一个复合视频剪辑，首先包含原始视频
        final_clip = video_clip.copy()

        # 在指定时间插入图片剪辑
        # 注意：insertion_time 需要是 float 类型，表示视频开始后的时间（秒）
        final_clip = CompositeVideoClip([final_clip, image_clip], size=video_clip.size)

        # 将图片剪辑插入到指定时间点
        final_clip = final_clip.crossfadein(duration).set_start(insertion_time)

        # 写入输出视频
        final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac', fps=video_clip.fps)


def create_video_from_images(image_paths_with_times, fps, output_video_path):
    # 创建一个空的剪辑列表用于存储视频片段
    clips = []

    # 将图片转换为视频片段，并根据时间合并它们
    for image_path, time in image_paths_with_times:
        image_clip = ImageClip(image_path).set_duration(time)
        clips.append(image_clip)

    # 合并所有视频片段为一个视频
    video_from_images = CompositeVideoClip(clips)

    # 设置视频的帧率
    video_from_images = video_from_images.set_fps(fps)

    video_from_images.write_videofile(output_video_path, codec='libx264', audio_codec='aac')

    return video_from_images


def merge_videos(video1, video2, position1, position2, output_video_path):
    # 设置两个视频的大小
    width = 607  # 假设视频宽度为1920像素
    height = 1080  # 假设视频高度为1080像素
    #video1 = video1.resize(width, height)
    #video2 = video2.resize(width, height)

    # 将两个视频合成一个视频
    final_clip = CompositeVideoClip([video1.set_position(position1),
                                     video2.set_position(position2)], size=(width, height))

    # 写入输出视频
    final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac')


"""
    将字幕合并到视频中。

    参数:
    - video_path: 视频文件的路径。
    - subtitle_path: 字幕文件的路径（目前支持SRT格式）。
    - output_video_path: 输出视频文件的路径。
    - position: 字幕在视频中的位置（'top', 'center', 'bottom'）。
    - font_size: 字幕的字体大小。
    - color: 字幕的颜色。
"""
def add_subtitles_to_video(video_path, subtitle_path, output_video_path, position='bottom', font_size=24,
                           color='white'):

    # 加载视频
    video_clip = VideoFileClip(video_path)

    # 加载字幕
    with open(subtitle_path, 'r', encoding='utf-8') as f:
        subtitles = f.read()

    # 创建字幕剪辑
    subtitle_clip = SubtitleClip(subtitles, font_name="Arial", font_size=font_size, color=color, position=position)

    # 将视频和字幕合并
    final_clip = CompositeVideoClip([video_clip, subtitle_clip], size=video_clip.size)

    # 写入输出视频
    final_clip.write_videofile(output_video_path, codec='libx264', audio_codec='aac', fps=video_clip.fps)

    # 释放资源
    video_clip.close()
    subtitle_clip.close()
    final_clip.close()


if __name__ == '__main__':
    current_directory = os.getcwd()  # 获取当前工作目录
    target_directory = os.path.join(current_directory, 'video')
    # 确保目标文件夹存在
    #os.makedirs(target_directory, exist_ok=True)

    target_directory = "J:\PycharmProjects\\AI-BOTS\\video\\"
    # 指定目标文件路径
    target_path = os.path.join(target_directory, 'cyk.mp4')
    # 指定修改背景的目标文件路径
    bg_target_path = os.path.join(target_directory, 'bg_cyk_app.mp4')
    # 指定修改背景的目标文件路径
    bg_target2_path = os.path.join(target_directory, 'bg_cyk_app2.mp4')
    # 指定修改背景图片路径
    bgpic_target_path = os.path.join(target_directory, 'bg.jpg')

    # 执行拷贝操作
    try:

        # 使用示例
        # 假设 image_paths_with_times 是一个包含图片路径和持续时间的列表
        image_paths_with_times = [
            (target_directory + 'bg.jpg', 25),  # 图片1持续5秒
            (target_directory + 'bg2.jpg', 10),  # 图片1持续5秒
            # 添加更多图片和持续时间...
        ]

        # 创建一个根据图片剧本生成的视频
        video_from_images = create_video_from_images(image_paths_with_times, fps=24,
                                                     output_video_path=bg_target_path)

        # 加载口播视频
        talking_video_path = target_path
        talking_clip = VideoFileClip(talking_video_path)

        # 合并视频，假设我们要将图片剧本生成的视频放在左侧，口播视频放在右侧
        merge_videos(video_from_images, talking_clip, position1=(0, 0), position2=(0, 200),
                     output_video_path=bg_target2_path)

        # 释放资源
        talking_clip.close()
        video_from_images.close()

        #insert_multiple_images(target_path, image_paths_with_times_and_positions, bg_target_path)

        #output_bg_video_path = composite_background_with_moviepy(bgpic_target_path, target_path, bg_target_path)
        #print(f"插入背景图片的视频成功拷贝到 {output_bg_video_path}")

    except Exception as e:
        print(f"拷贝文件时发生错误: {e}")