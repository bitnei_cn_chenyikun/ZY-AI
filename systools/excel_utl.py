import pandas as pd

# 读取Excel文件
file_path = 'C:\\Users\\Administrator\WPSDrive\\587324438_1\WPS云盘\智云科技经营资料\财务数据\广东智云城建科技有限公司-银行日记账2024年03月(1).xlsx'  # 替换为你的Excel文件路径
new_file_path = 'C:\\Users\\Administrator\WPSDrive\\587324438_1\WPS云盘\智云科技经营资料\财务数据\广东智云城建科技有限公司-银行日记账2024年03月(1)_new.xlsx'  # 替换为你的Excel文件路径
all_sheets = pd.read_excel(file_path, sheet_name=None)

# 排除“银行汇总”工作表
sheets_to_combine = {k: v for k, v in all_sheets.items() if k != '银行汇总'}

# 定义需要合并的列名
columns_to_merge = ['日期', '银收编号', '银付编号', '名称', '类型', '部门', '摘要', '收入', '支出']

# 创建一个新的DataFrame用于存储合并后的数据
combined_data = pd.DataFrame()

# 遍历所有工作表并合并指定的列
for sheet_name, data in sheets_to_combine.items():
    # 删除每个工作表的第一行
    data = data.iloc[1:]

    # 选择指定的列
    selected_data = data[columns_to_merge]

    # 添加来源列
    selected_data['来源'] = sheet_name

    # 使用pandas.concat来合并数据
    combined_data = pd.concat([combined_data, selected_data], ignore_index=True)

# 将合并后的数据保存到一个新的工作表“合并数据”
with pd.ExcelWriter(new_file_path, engine='openpyxl') as writer:
    combined_data.to_excel(writer, sheet_name='合并数据', index=False)

print("合并完成，已保存到新文件中。")