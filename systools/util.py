import codecs
import os
import sys
import random
import time
import configparser
import os

from systools import wsa_server
from schedulers.thread_manager import MyThread
from systools import config_util

# 获取当前执行脚本的目录
current_script_directory = os.path.dirname(os.path.abspath(__file__))

# 获取当前工作目录
current_working_directory = os.getcwd()

LOGS_FILE_URL = "logs/log-" + time.strftime("%Y%m%d%H%M%S") + ".log"

def update_config(file_path, section, key, new_value):
    config = configparser.ConfigParser()
    config.read(file_path)
    config.set(section, key, new_value)
    with open(file_path, 'w') as configfile:
        config.write(configfile)

def find_media_file(directory, extensions):
    for file_name in os.listdir(directory):
        if any(file_name.lower().endswith(ext) for ext in extensions):
            return os.path.join(directory, file_name)
    return None

def find_image_or_video(in_folder):
    allowed_extensions = ['.jpeg', '.jpg', '.png', '.bmp', '.mp4', '.avi', '.mov', '.mkv']
    return find_media_file(in_folder, allowed_extensions)

def find_audio(in_folder):
    allowed_extensions = ['.mp3', '.wav', '.aac']
    return find_media_file(in_folder, allowed_extensions)

def random_hex(length):
    result = hex(random.randint(0, 16 ** length)).replace('0x', '').lower()
    if len(result) < length:
        result = '0' * (length - len(result)) + result
    return result


def __write_to_file(text):
    if not os.path.exists("logs"):
        os.mkdir("logs")
    file = codecs.open(LOGS_FILE_URL, 'a', 'utf-8')
    file.write(text + "\n")
    file.close()


def printInfo(level, sender, text, send_time=-1):
    if send_time < 0:
        send_time = time.time()
    format_time = time.strftime('%H:%M:%S', time.localtime(send_time))
    logStr = '[{}][{}] {}'.format(format_time, sender, text)
    print(logStr)
    if level >= 3:
        wsa_server.get_web_instance().add_cmd({"panelMsg": text})
        if not config_util.config["interact"]["playSound"]: # 非展板播放
            content = {'Topic': 'Unreal', 'Data': {'Key': 'log', 'Value': text}}
            wsa_server.get_instance().add_cmd(content)
    MyThread(target=__write_to_file, args=[logStr]).start()


def log(level, text):
    printInfo(level, "系统", text)

class DisablePrint:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout