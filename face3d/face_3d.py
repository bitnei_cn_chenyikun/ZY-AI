
import cv2
import mediapipe as mp
import numpy as np
import pyqtgraph.opengl as gl
from PyQt5 import QtWidgets

# 初始化 MediaPipe Face Mesh 模型
mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh()

# 读取图片
image_file = '../video/women1.png'  # 将此路径替换为你的图片文件路径
image = cv2.imread(image_file)
h, w, _ = image.shape

# 转换图片颜色空间从 BGR 到 RGB
rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# 使用 Face Mesh 处理图片，并得到结果
results = face_mesh.process(rgb_image)

# 准备用于三维重建的点
mesh_points = np.array(
    [[res.x * w, res.y * h, res.z * 1000] for res in results.multi_face_landmarks[0].landmark]
)

# 创建一个应用窗口
app = QtWidgets.QApplication([])

# 创建一个GLViewWidget对象
widget = gl.GLViewWidget()
widget.show()

# 创建一个plot对象
scatter_plot = gl.GLScatterPlotItem(pos=mesh_points, color=(1, 1, 1, 1), size=2)
widget.addItem(scatter_plot)

# 启动应用
if __name__ == '__main__':
    app.exec_()