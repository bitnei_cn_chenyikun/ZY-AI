import concurrent
import json
import math
import time
from langchain_core.tools import Tool

import qa_service
from llm import llm_rwkv_api, llm_zhipu_api, llm_kimi_api, llm_gpt_api
from tts.TTS import EdgeTTS

from agent.plugins.MyTimer import MyTimer
from agent.plugins.Weather import Weather
from agent.plugins.CheckSensor import CheckSensor
from agent.plugins.Switch import Switch
from agent.plugins.Knowledge import Knowledge
from agent.plugins.Say import Say
from agent.plugins.QueryTimerDB import QueryTimerDB
from agent.plugins.DeleteTimer import DeleteTimer
from agent.plugins.GetSwitchLog import GetSwitchLog
from agent.plugins.getOnRunLinkage import getOnRunLinkage
from agent.plugins.QueryTime import QueryTime
from agent.plugins.PythonExecutor import PythonExecutor
from agent.plugins.WebPageRetriever import WebPageRetriever
from agent.plugins.WebPageScraper import WebPageScraper

class GlobalSingleton:
    agents = None
    title = ""
    tts = None
    executor = None
    system_data = None
    llm_modules_cmb = None
    llm_modules = None

    my_timer = None
    weather_tool = None
    check_sensor_tool = None
    switch_tool = None
    knowledge_tool = None
    say_tool = None
    query_time = None
    query_timer_db_tool = None
    delete_timer_tool = None
    get_switch_log = None
    get_on_run_linkage = None
    python_executor = None
    web_page_retriever = None
    web_page_scraper = None

    def get_agent_by_name(self, agent_name):
        for index, agent in enumerate(self.agents):
            if agent_name == agent.name:
                return agent
        return None

    def llm_talk(self, llm_module, msg):
        text = ''
        action_json = None
        vedio = ''
        type = ''
        textlist = []
        try:
            print('llm大模型处理...')
            tm = time.time()

            # 首先采用本地QA知识库应答
            # 人设问答
            #keyword = qa_service.question('Persona', msg)
            #if keyword is not None:
            #    print('先忽略人设回答')
                # text = config_util.config["attribute"][keyword]

            # 全局问答
            if text is None or text == '':
                print('[!] qa检索！')
                answer, action, similar, vedio_path = qa_service.question('qa', msg)
                vedio = vedio_path
                if action:
                    action_json = json.loads(action)

                if answer is None and action is None:
                    # 采用大模型LLM
                    selected_module = self.llm_modules.get(llm_module)
                    if selected_module is None:
                        raise RuntimeError('llm大模型没有配置！')

                    text = selected_module.question(msg)
                    type = 'llm'

                else:
                    text = answer
                    type = 'qa'

            print('llm大模型处理完成. 耗时: {} ms'.format(math.floor((time.time() - tm) * 1000)))
            if text == '哎呀，你这么说我也不懂，详细点呗' or text == '':
                print('[!] 大模型无语了！')
                text = '哎呀，你这么说我也不懂，详细点呗'
        except BaseException as e:
            print(e)
            print('llm大模型处理错误！')
            text = '哎呀，你这么说我也不懂，详细点呗'

        return text, action_json, vedio, type

    def __init__(self):
        self.title = "智云AI平台"
        self.tts = EdgeTTS()
        # 初始化线程池
        self.executor = concurrent.futures.ThreadPoolExecutor()
        self.system_data = {'ProjectValue': "8080", 'StructuralConstructionProgress': "80", 'MechanicalProgress': "80",
                       'InteriorFinishingProgress': "80"}

        self.llm_modules_cmb = ['llm_chatglm3', 'llm_rwkv_api', 'llm_kimi_api',  'llm_gpt3.5']
        self.llm_modules = {
            # "nlp_yuan": nlp_yuan,
            "llm_gpt3.5": llm_gpt_api,
            # "llm_gpt4.0": nlp_gpt,
            # "nlp_rasa": nlp_rasa,
            # "nlp_VisualGLM": nlp_VisualGLM,
            # "nlp_lingju": nlp_lingju,
            "llm_rwkv_api": llm_rwkv_api,  # 采用rwkv的API大模型服务
            "llm_kimi_api": llm_kimi_api,  # 采用kimi的API大模型服务
            "llm_chatglm3": llm_zhipu_api,
            # "nlp_fastgpt": nlp_fastgpt

        }

        # 创建agent chain
        self.my_timer = MyTimer()
        self.weather_tool = Weather()
        self.check_sensor_tool = CheckSensor()
        self.switch_tool = Switch()
        self.knowledge_tool = Knowledge()
        self.say_tool = Say()
        self.query_time = QueryTime()
        self.query_timer_db_tool = QueryTimerDB()
        self.delete_timer_tool = DeleteTimer()
        self.get_switch_log = GetSwitchLog()
        self.get_on_run_linkage = getOnRunLinkage()
        self.python_executor = PythonExecutor()
        self.web_page_retriever = WebPageRetriever()
        self.web_page_scraper = WebPageScraper()

        self.agents = [
            Tool(
                name=self.python_executor.name,
                func=self.python_executor.run,
                description=self.python_executor.description
            ),
            Tool(
                name=self.my_timer.name,
                func=self.my_timer.run,
                description=self.my_timer.description
            ),
            Tool(
                name=self.weather_tool.name,
                func=self.weather_tool.run,
                description=self.weather_tool.description
            ),
            Tool(
                name=self.check_sensor_tool.name,
                func=self.check_sensor_tool.run,
                description=self.check_sensor_tool.description
            ),
            Tool(
                name=self.switch_tool.name,
                func=self.switch_tool.run,
                description=self.switch_tool.description
            ),
            Tool(
                name=self.knowledge_tool.name,
                func=self.knowledge_tool.run,
                description=self.knowledge_tool.description
            ),
            Tool(
                name=self.say_tool.name,
                func=self.say_tool.run,
                description=self.say_tool.description
            ),
            Tool(
                name=self.query_time.name,
                func=self.query_time.run,
                description=self.query_time.description
            ),
            Tool(
                name=self.query_timer_db_tool.name,
                func=self.query_timer_db_tool.run,
                description=self.query_timer_db_tool.description
            ),
            Tool(
                name=self.delete_timer_tool.name,
                func=self.delete_timer_tool.run,
                description=self.delete_timer_tool.description
            ),
            Tool(
                name=self.get_switch_log.name,
                func=self.get_switch_log.run,
                description=self.get_switch_log.description
            ),
            Tool(
                name=self.get_on_run_linkage.name,
                func=self.get_on_run_linkage.run,
                description=self.get_on_run_linkage.description
            ),
            Tool(
                name=self.web_page_retriever.name,
                func=self.web_page_retriever.run,
                description=self.web_page_retriever.description
            ),
            Tool(
                name=self.web_page_scraper.name,
                func=self.web_page_scraper.run,
                description=self.web_page_scraper.description
            )
            # ,Tool(
            #    name=knowledge_base_responder.name,
            #    func=knowledge_base_responder.run,
            #    description=knowledge_base_responder.description
            # )
        ]


__instance: GlobalSingleton = None

def new_instance() -> GlobalSingleton:
    global __instance
    if __instance is None:
        __instance = GlobalSingleton()
    return __instance

def get_instance() -> GlobalSingleton:
    return __instance

