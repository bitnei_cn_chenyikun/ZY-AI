import os
import gradio as gr
from TTS.api import TTS
import librosa

title = "文本转语音"


def generateAudio(text):
    # 由于TTS无法很好地处理回车符和空格，需要对text里的回车符进行替换
    text = text.replace("\n", ",")
    text = text.replace(" ", "")
    tts = TTS(model_name="tts_models/zh-CN/baker/tacotron2-DDC-GST", progress_bar=True, gpu=False)
    tts.tts_to_file(text, file_path="output.wav")

    audio, sr = librosa.load(path="output.wav")

    return sr, audio


app = gr.Interface(
    fn=generateAudio,
    inputs="text",
    outputs="audio",
    title=title,
    examples=[os.path.join(os.path.dirname(__file__), "output.wav")]
)

app.launch()
