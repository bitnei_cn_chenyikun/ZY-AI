import os

import whisper

import qa_service
import subprocess

from run import run_easy_wab2lip
from tts.asr_whisper import asr_whisper

def AutoVedioCreate(vedio_name="video\cyk.mp4"):
    output_video, temp_output = run_easy_wab2lip(vedio_name, "Enhanced")
    print("数字人生成成功,{}".format(temp_output))
    return temp_output

def add_subtitles_to_video(video_path, subtitle_path, output_path):
    """
    将VTT字幕文件合并到MP4视频文件中。 ffmpeg -i cyk_tts_Easy-Wav2Lip.mp4 -i cyk_tts_Easy-Wav2Lip.srt -c copy -c:s mov_text output.mp4

    :param video_path: 视频文件的路径
    :param subtitle_path: 字幕文件的路径
    :param output_path: 输出文件的路径
    """
    # 构建ffmpeg命令
    command = [
        'ffmpeg', '-i', video_path,  # 输入视频文件
        '-i', subtitle_path,  # 输入字幕文件
        '-c:v', 'copy',  # 复制视频流
        '-c:a', 'copy',  # 复制音频流
        '-c:s', 'mov_text',  # 使用mov_text字幕编解码器
        '-map', '0',  # 将所有流映射到输出文件
        '-map', '1',  # 将字幕流映射到输出文件
        output_path  # 输出文件路径
    ]

    # 执行ffmpeg命令
    subprocess.run(command)

# 使用示例
video_path = 'video/cyk_tts_Easy-Wav2Lip.mp4'
subtitle_path = 'video/cyk_tts_Easy-Wav2Lip.srt'
output_path = 'video/output.mp4'
audio_path = 'video/tts.wav'


def generate_subtitles(audio_path, video_path, subtitle_format='srt'):
    """
    生成与视频文件同名的字幕文件。

    参数:
    - audio_path: 音频文件的路径
    - video_path: 视频文件的路径
    - subtitle_format: 字幕文件的格式 ('srt' 或 'vtt')
    """
    # 加载 Whisper 模型
    model = whisper.load_model("large-v3")

    # 对音频文件进行转录
    result = model.transcribe(audio_path)

    # 获取视频文件的名称（不包含扩展名）
    video_filename = os.path.splitext(os.path.basename(video_path))[0]

    # 根据所需的格式生成字幕文件
    if subtitle_format not in ['srt', 'vtt']:
        raise ValueError("Subtitle format must be 'srt' or 'vtt'")

    # 构建字幕文件的完整路径
    subtitle_path = f"{video_filename}.{subtitle_format}"

    # 获取字幕文件所在的目录
    output_dir = os.path.dirname(video_path)

    # 使用 Whisper 的 get_writer 方法生成字幕文件
    writer = whisper.utils.get_writer(subtitle_format, output_dir)
    writer(result, subtitle_path)

    print(f"Subtitles generated: {subtitle_path}")


if __name__ == '__main__':
    AutoVedioCreate()

    #text = "目前已累计产值是8342万。"
    #output_video = 'video\cyk_tts_Easy-Wav2Lip.mp4'
    #qa_service.set_answer_video(text, output_video)

    #先生成字幕
    #generate_subtitles(audio_path, video_path)

    #测试字幕写入mp4
    #add_subtitles_to_video(video_path, subtitle_path, output_path)

