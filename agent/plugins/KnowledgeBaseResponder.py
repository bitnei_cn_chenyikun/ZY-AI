import os
from typing import Any

import chromadb
import numpy as np
from langchain_text_splitters import RecursiveCharacterTextSplitter

from systools import config_util as cfg
from core import content_db
from langchain.tools import BaseTool
from langchain.document_loaders import PyPDFLoader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.indexes.vectorstore import VectorstoreIndexCreator, VectorStoreIndexWrapper
from langchain.vectorstores.chroma import Chroma
from langchain.chat_models import ChatOpenAI
import hashlib
import pdfplumber

index_name = "knowledge_data"
folder_path = "agent/plugins/KnowledgeBaseResponder/knowledge_base"
local_persist_path = "agent/plugins/KnowledgeBaseResponder"
md5_file_path = os.path.join(local_persist_path, "pdf_md5.txt")

class KnowledgeBaseResponder(BaseTool):
    name = "KnowledgeBaseResponder"
    description = """此工具用于连接本地知识库获取问题答案，使用时请传入相关问题作为参数，例如：“草梅最适合的生长温度”"""

    def __init__(self):
        super().__init__()

    async def _arun(self, *args: Any, **kwargs: Any) -> Any:
        # 用例中没有用到 arun 不予具体实现
        pass


    def _run(self, para: str) -> str:
        self.save_all()
        result = self.question(para)
        return result

    def generate_file_md5(self, file_path):
        hasher = hashlib.md5()
        with open(file_path, 'rb') as afile:
            buf = afile.read()
            hasher.update(buf)
        return hasher.hexdigest()

    def load_md5_list(self):
        if os.path.exists(md5_file_path):
            with open(md5_file_path, 'r') as file:
                return {line.split(",")[0]: line.split(",")[1].strip() for line in file}
        return {}

    def update_md5_list(self, file_name, md5_value):
        md5_list = self.load_md5_list()
        md5_list[file_name] = md5_value
        with open(md5_file_path, 'w') as file:
            for name, md5 in md5_list.items():
                file.write(f"{name},{md5}\n")

    def load_all_pdfs(self, folder_path):
        md5_list = self.load_md5_list()
        for file_name in os.listdir(folder_path):
            if file_name.endswith(".pdf"):
                file_path = os.path.join(folder_path, file_name)
                file_md5 = self.generate_file_md5(file_path)
                if file_name not in md5_list or md5_list[file_name] != file_md5:
                    print(f"正在加载 {file_name} 到索引...")
                    self.load_pdf_and_save_to_index(file_path, index_name)
                    self.update_md5_list(file_name, file_md5)

    def get_index_path(self, index_name):
        return os.path.join(local_persist_path, index_name)

    def load_pdf_and_save_to_index(self, file_path, index_name):
        loader = PyPDFLoader(file_path)
        embedding = OpenAIEmbeddings(model="text-embedding-ada-002", openai_api_key='sk-NYsoG3VBKDiTuvdtC969F95aFc4f45379aD3854a93602327')
        index = VectorstoreIndexCreator(embedding=embedding, vectorstore_kwargs={"persist_directory": self.get_index_path(index_name)}).from_loaders([loader])
        index.vectorstore.persist()

    def load_index(self, index_name):
        index_path = self.get_index_path(index_name)
        embedding = OpenAIEmbeddings(model="text-embedding-ada-002", openai_api_key='sk-NYsoG3VBKDiTuvdtC969F95aFc4f45379aD3854a93602327')
        vectordb = Chroma(persist_directory=index_path, embedding_function=embedding)
        return VectorStoreIndexWrapper(vectorstore=vectordb)

    def save_all(self):
        self.load_all_pdfs(folder_path)

    def question(self, cont):
        try:
            info = cont
            index = self.load_index(index_name)    
            llm = ChatOpenAI(model="gpt-4-0125-preview")
            ans = index.query(info, llm, chain_type="map_reduce")
            return ans
        except Exception as e:
            print(f"请求失败: {e}")
            return "抱歉，我现在太忙了，休息一会，请稍后再试。"

    def pdf_to_text(self, pdf_path):
        with pdfplumber.open(pdf_path) as pdf:
            text = ""
            for page in pdf.pages:
                text += page.extract_text()
        return text

    def test_pdf_files(self, folder_path):
        pdf_files = [folder_path+"\\《建筑工程设计文件编制深度规定》2016年版(1).pdf"]

        # 将PDF文件转换为文本并添加到DocumentStore中
        for pdf_file in pdf_files:
            #text = self.pdf_to_text(pdf_file)
            #加载文档
            loader = PyPDFLoader(pdf_file)
            pages = loader.load()
            print('pages:{}'.format(str(len(pages))))

            #开始切割
            r_splitter = RecursiveCharacterTextSplitter(
                chunk_size=26,  # 块长度
                chunk_overlap=4  # 重叠字符串长度
            )

            splits  = r_splitter.split_documents(pages)

            print('splitter docs:{}'.format(str(len(splits ))))

            #向量的转换和存储
            embedding = OpenAIEmbeddings(openai_api_key='sk-NYsoG3VBKDiTuvdtC969F95aFc4f45379aD3854a93602327', openai_api_base='https://key.wenwen-ai.com/v1')
            sentence1 = "我喜欢小狗。"
            sentence2 = "我喜欢小动物。"
            sentence3 = "我今天心情很差。"
            embedding1 = embedding.embed_query(sentence1)
            embedding2 = embedding.embed_query(sentence2)
            embedding3 = embedding.embed_query(sentence3)
            dot_result = np.dot(embedding1, embedding2)
            print('dot_result:{}'.format(str(dot_result)))

            print(embedding1)

            # 向量数据库保存位置
            persist_directory = folder_path+'/docs/chroma/'

            # 创建向量数据库
            vectordb = Chroma.from_documents(
                documents=splits,
                embedding=embedding,
                persist_directory=persist_directory
            )

            # 查看向量数据库中的文档数量
            print(vectordb._collection.count())

            question = "请说一下系统造价的计算方法？"

            docs = vectordb.similarity_search(question, k=3)

            # 打印文档数量
            print(len(docs))
            for doc in docs:
                print(doc.page_content)

            #document = {"text": text, "meta": {"file_name": pdf_file}}
            #document_store.write_documents([document])

    def read_vectordb(self, folder_path):

        # 向量数据库保存位置
        persist_directory = folder_path + '/docs/chroma/'

        # 从文件中读取向量数据
        client = chromadb.PersistentClient(path=persist_directory)

        # 相似度搜索
        question = "请说一下系统造价的计算方法？"
        collection = client.get_collection(name='langchain')
        query_result = collection.query(query_texts=question)
        print(query_result)

if __name__ == "__main__":
    index_name = "knowledge_data"
    folder_path = "KnowledgeBaseResponder\\knowledge_base"
    local_persist_path = ""
    md5_file_path = os.path.join(local_persist_path, "pdf_md5.txt")

    cfg.load_config('../../system.conf', '../../config.json')
    contentdb = content_db.new_instance('../fay.db')

    tool = KnowledgeBaseResponder()
    #tool.test_pdf_files(folder_path)
    tool.read_vectordb(folder_path)
    #info = tool.run("草莓最适合的生长温度")
    #print(info)
