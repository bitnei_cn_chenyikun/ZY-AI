import os
import math

from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.chat_models import ChatOpenAI
from langchain.memory import VectorStoreRetrieverMemory
import faiss
from langchain.docstore import InMemoryDocstore
from langchain.vectorstores import FAISS
from langchain.agents import Tool, initialize_agent, agent_types
from langchain_community.vectorstores.docarray import DocArrayInMemorySearch
from langchain_core.embeddings import Embeddings
from transformers import AutoModel, AutoTokenizer

import systools.config_util as utils
from llm import llm_rwkv_api
from systools import util


class AiAgentCore():
    def __init__(self):
        utils.load_config()



        #os.environ['OPENAI_API_KEY'] = utils.key_chatgpt_api_key
        #os.environ['OPENAI_API_BASE'] = utils.key_gpt_base_url

        # 创建llm
        #self.llm = ChatOpenAI(model="gpt-3.5-turbo", verbose=True)

        #使用open ai embedding
        #embedding_size = 1536  # OpenAIEmbeddings 的维度
        #index = faiss.IndexFlatL2(embedding_size)
        #embedding_fn = OpenAIEmbeddings()



        #embedding_fn = HuggingFaceBgeEmbeddings(model_name="BAAI/bge-large-zh-v1.5")

        #vectordb = DocArrayInMemorySearch.from_texts(
        #    ["青蛙是食草动物",
        #     "人是由恐龙进化而来的。"
        #     ],
        #    embedding=embedding_fn
        #)

        # #创建检索器
        #bge_retriever = vectordb.as_retriever(search_kwargs={"k": 1})
        #print(bge_retriever.get_relevant_documents("恐龙"))

        #from langchain.schema.runnable import RunnableMap
        #from langchain.prompts import ChatPromptTemplate
        #from langchain.schema.output_parser import StrOutputParser
        #from langchain_google_genai import ChatGoogleGenerativeAI

        # 创建model
        #google_api_key = 'your_api_key_here'
        #model = ChatGoogleGenerativeAI(model="gemini-pro", google_api_key=google_api_key)

        # 创建prompt模板
        #template = """Answer the question a full sentence,
        #based only on the following context:
       # {context}
        #Question: {question}
        #"""

        # 由模板生成prompt
        #prompt = ChatPromptTemplate.from_template(template)

        # 创建chain
        #chain = RunnableMap({
        #    "context": lambda x: bge_retriever.get_relevant_documents(x["question"]),
        #    "question": lambda x: x["question"]
        #}) | prompt | model | StrOutputParser()

        #response = chain.invoke({"question", "人从哪里来?"})
        #print (response)



        #内存保存聊天历史
        self.chat_history = []

        #记录一轮执行有无调用过say tool
        self.is_use_say_tool = False   
        self.say_tool_text = ""

        self.total_tokens = 0
        self.total_cost = 0

    #记忆prompt
    def format_history_str(self, str):
        result = ""
        history_string = str['history']

        # Split the string into lines
        lines = history_string.split('input:')

        # Initialize an empty list to store the formatted history
        formatted_history = []

        #处理记忆流格式
        for line in lines:
            if "output" in line:
                input_line = line.split("output:")[0].strip()
                output_line = line.split("output:")[1].strip()
                formatted_history.append({"input": input_line, "output": output_line})

        
        # 记忆流转换成字符串
        result += "-以下是与用户说话关连度最高的记忆：\n"
        for i in range(len(formatted_history)):
            if i >= 3:
                break
            line = formatted_history[i]
            result += f"--input：{line['input']}\n--output：{line['output']}\n"
        if len(formatted_history) == 0:
            result += "--没有记录\n"


        #添加内存记忆
        formatted_history = []
        for line in self.chat_history:
            formatted_history.append({"input": line[0], "output": line[1]})
        
        #格式化内存记忆字符串
        result += "\n-以下刚刚的对话：\n"
        for i in range(len(formatted_history)):
            line = formatted_history[i]
            result += f"--input：{line['input']}\n--output：{line['output']}\n"
        if len(formatted_history) == 0:
            result += "--没有记录\n"

        return result
    
    def run(self, input_text):
        self.is_use_say_tool = False
        self.say_tool_text = ""
        
        chat_text = ""


        return self.is_use_say_tool, chat_text

if __name__ == "__main__":
    agent = AiAgentCore()
    print(agent.run("你好"))
